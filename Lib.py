# Example lib showcasing the different features
import math

lib = [
  {
    "inputs"   : ["Hello", "Hi"], # Messages to respond to
    "req_ping" : False,           # Require mention of bot name in message? ### OPTIONAL
    "flags_req" : ["greetings"],  # Array of set flags needed to use this response thing ### OPTIONAL
    "req_rep" : (-5, math.inf),   # Reputation range to reply in, use math.inf for infinite numbers - format: (min, max) ### OPTIONAL
    "chg_rep" : -1,               # How to affect message sender's reputation ### OPTIONAL
    "replies"  : [                # List of reply blocks
    {
      "text" : "Hello, {ping}.",  # Message to send
      "func" : "",                # Function output to add to message, will replace "{func}" ### OPTIONAL
      "ping" : True               # Mention the original sender? Will replace "{ping}" with name ### OPTIONAL
    },
    {
      "text" : "Hi.",
      "func" : "",
      "ping" : False
    },
    {
      "text" : "Hello.",
      "func" : "",
      "ping" : False
    },
    {
      "text" : "Hello. How are you?",
      "flags_set" : ["how_are_you"],    # Array of flags to set on this response, flags are auto-cleared after 120 seconds ### OPTIONAL
    }
    ]
  },
  {
    "inputs"   : ["Flip a Coin"],                     # Messages to respond to
    "req_ping" : True,                                # Require mention of bot name in message?
    "replies"  : [                                    # List of reply blocks
    {
      "text" : "It landed on {func}.",                # Message to send
      "func" : "random.choice(['heads', 'tails'])",   # Function output to add to message, will replace "{func}"
      "ping" : False                                  # Mention the original sender? Will replace "{ping}" with name
    },
    ]
  },
]