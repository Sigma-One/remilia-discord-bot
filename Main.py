# Work with Python 3.6
import discord
import Tokens
import Chat
import Lib
import asyncio
import time
import random

# Main Client class
class MyClient(discord.Client):
    # Runs once client is connected
    async def on_ready(self):
        print('Connected to Discord')
        print('Username: {0.name}\nID: {0.id}'.format(self.user))
        #GENERAL_CHAN = self.get_channel(490274694692339713)
        #msg = await GENERAL_CHAN.send(random.choice(Lib.ready_msgs))

    # Runs on received message
    async def on_message(self, message):
        if message.content.startswith('!test_helios'):
            msg = await message.channel.send('Passed')

        else:
          try:
            # Send response if any are available
            msg = await message.channel.send(Chat.Main(message, self.user))
          except discord.errors.HTTPException:
            print("WARN: No Reply")

# Run the client
client = MyClient()
client.run(Tokens.DISCORDBOT_T)
