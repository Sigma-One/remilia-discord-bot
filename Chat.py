import difflib
import random
import Lib
import Reps
import time
import re
from math import floor

import datetime

flags = {}

def Main(message, myself):
  # Make sure that message is not from myself
  if message.author.name != Lib.Username:

    # Clean up expired flags
    flagkeys = []
    for i in flags.keys():
      flagkeys.append(i)
    for i in flagkeys:
      if floor(time.time()) - flags[i] > 120:
        flags.pop(i, None)
    print(flags)

    # Make sure author's reputation is tracked
    if message.author.id not in Reps.user_reps.keys():
      Reps.add_rep_user(message.author.id)

    # Set similarity ratio to 0
    last_ratio = 0

    # Loop through library inputs
    for i in Lib.lib:
      for j in i["inputs"]:

        # Check input similarity to library after lowercasing everything and removing own name
        sim_seq = difflib.SequenceMatcher(None, re.sub('[^a-z0-9\ ]+', '', message.content.lower().replace(myself.name.lower(), "").replace(str(myself.id), "")), j.lower())
        # Make the current library entry the candidate for reply
        candidate = i
        # Print current comparison
        #print(message.content.lower().replace(myself.name.lower(), "").replace(str(myself.id), ""), " vs ", j, sim_seq.ratio()*100, " vs ", last_ratio)

        # Compare candidate similarity score to previous high
        if sim_seq.ratio()*100 > last_ratio:
          # Proceed only if I am mentioned or a mention is not required
          if any(c in message.content.lower() for c in (myself.name.lower(), str(myself.id))) or "req_ping" not in candidate.keys() or not candidate["req_ping"]:
            # Make sure reputation is good
            if "req_rep" not in candidate.keys() or Reps.user_reps[message.author.id] <= candidate["req_rep"][1] and Reps.user_reps[message.author.id] >= candidate["req_rep"][0]:
              # Make sure the correct flags are set
              if "flags_req" not in candidate.keys() or all(i in flags.keys() for i in candidate["flags_req"]):
                # Set new best score and response
                last_ratio = sim_seq.ratio()*100
                response = candidate

    # Only reply is best score is over 75
    if last_ratio > 75:

      # Randomly choose a reply
      resp_choice = random.choice(response["replies"])
      resp_text = resp_choice["text"]

      # Mention the original message author if needed
      if "{ping}" in resp_choice["text"]:
        resp_text = resp_text.format(ping=message.author.name)

      # Add any function outputs if needed
      if "func" in resp_choice.keys() and resp_choice["func"]:
        resp_text = resp_text.format(func=eval(resp_choice["func"]))

      # Set flags if necessary
      if "flags_set" in resp_choice.keys():
        for i in resp_choice["flags_set"]:
          flags[i] = floor(time.time())

      if "flags_req" in response.keys():
        for i in response["flags_req"]:
          flags.pop(i, None)

      # Modify user reputation
      if "chg_rep" in response.keys():
        Reps.change_rep(message.author.id, response["chg_rep"])

      # Return the chosen response
      return resp_text
