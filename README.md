A discord bot capable of library-based chatting and simple functions.

Dependencies (Python Modules):

* difflib
* discord.py (async branch. install with `$ sudo python3 -m pip install -U https://github.com/Rapptz/discord.py/archive/rewrite.zip#egg=discord.py[voice]`)

Installation:

1. Make sure you have all the dependencies installed
2. Run the thing with `python3 Main.py`
