import json

# Read reps on startup
with open('repfile.json', 'r') as repfile:
  raw_json = json.load(repfile)
  user_reps = {}
  for i in raw_json.keys():
    user_reps[int(i)] = raw_json[i]

# Updates reputation file
def update_rep_file():
  with open('repfile.json', 'w') as repfile:
    json.dump(user_reps, repfile)

# Adds new user to be tracked
def add_rep_user(user):
  user_reps[user] = 0
  update_rep_file()

# Modifies user's reputation
def change_rep(user, amount):
  user_reps[user] += amount
  update_rep_file()
